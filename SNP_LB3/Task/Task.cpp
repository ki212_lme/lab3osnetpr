#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <iostream>
#include <string>
#include "stdio.h"
#include "tchar.h"
#include <vector>
#include<stdio.h>
#include <inttypes.h>
#include<conio.h>
using namespace std;
int GetDriveLetter(vector<string> &drives) {
    char buffer[110];
    int len = ::GetLogicalDriveStrings(110, buffer);
    int start = 0;
    for (int i = 0; i < len; i++) {
        if (buffer[i] == '\0' && start != i) {
            drives.push_back(buffer + start);
            start = i + 1;;
        }
    }
    return 0;
}
int GetDrivesType(vector<string> &drives) {
    GetDriveLetter(drives);
    for (string x : drives) {
        switch (GetDriveType(x.c_str()))
        {
        case 0: 
            cout << "Диск " + x + " тип - Неможливо визначити.\n";
            break;
        case 1: 
            cout << "Диск " + x + " тип - Недійсний кореневий шлях/Недоступно.\n";
            break;
        case 2: 
            cout << "Диск " + x + " тип - Знімний.\n";
            break;
        case 3: 
            cout << "Диск " + x + " тип - Виправлено.\n";
            break;
        case 4: 
            cout << "Диск " + x + " тип - Мережа.\n";
            break;
        case 5: 
            cout << "Диск " + x + " тип - CD-ROM.\n";
            break;
        case 6: 
            cout << "Диск " + x + " тип - RAMDISK.\n";
            break;
        default: "Невідоме значення!\n";
        }
    }
    return 0;
}
int GetInfoAboutDrivers(vector<string> &drives) {
    GetDriveLetter(drives);
    for (string x : drives) {
        char VolumeName[100], FileSystemNameBuffer[100];
        DWORD SerialNumber, MaxLength;
        cout << endl;
        cout << "Диск: " + x << endl;
        GetVolumeInformation(x.c_str(), VolumeName, 100, &SerialNumber, &MaxLength, NULL, FileSystemNameBuffer, 100);
        cout << "Назва: " << VolumeName << endl;
        printf("Серійний номер: %d\n", SerialNumber);
        printf("Розмір: %d\n", MaxLength);
        cout << "Назва файлової системи: " << FileSystemNameBuffer << endl;
        cout << endl;
    }
    return 0;
}
int GetInfoAboutFreeSpace(vector<string>& drives) {
    GetDriveLetter(drives);
    for (string x : drives) {
        __int64 freeBytesForUsing, TotalSpace, TotalFreeSpace;
        cout << "Диск: " + x << endl;
        GetDiskFreeSpaceEx(x.c_str(), (PULARGE_INTEGER)&freeBytesForUsing, (PULARGE_INTEGER)&TotalSpace, (PULARGE_INTEGER)&TotalFreeSpace);
        printf("Вільне місце для даного користувача: %" PRId64 "\n", freeBytesForUsing);
        printf("Вільне місце для користувача: %" PRId64 "\n", TotalSpace);
        printf("Вільне місце на диску: %" PRId64 "\n\n", TotalFreeSpace);
    }
    return 0;
}
int GetInfoAboutSystemMemory(vector<string>& drives) {
    MEMORYSTATUS lpBuffer;
    GlobalMemoryStatus(&lpBuffer);
    printf("Розмір структури: %d\n", lpBuffer.dwLength);
    printf("Завантаженість: %d\n", lpBuffer.dwMemoryLoad);
    printf("Максимальне значення фізичної памяті: %zu\n", lpBuffer.dwTotalPhys);
    printf("Значення фізичної памяті: %zu\n", lpBuffer.dwAvailPhys);
    printf("Максимальне значення пам'яті для програм: %zu\n", lpBuffer.dwTotalPageFile);
    printf("Вільне значення пам'яті для програм: %zu\n", lpBuffer.dwAvailPageFile);
    printf("Максимальне значення віртуальної памяті: %zu\n", lpBuffer.dwTotalVirtual);
    printf("Вільне значення віртуальної памяті: %zu\n", lpBuffer.dwAvailVirtual);
    return 0;
}
int GetCompName() {
    char name[100];
    DWORD bufCharCount = 100;
    GetComputerName(name, &bufCharCount);
    _tprintf(TEXT("\nНазва компа: %s"), name);
    return 0;
}
int GetUsName() {
    char name[100];
    DWORD bufCharCount = 100;
    GetUserName(name, &bufCharCount);
    _tprintf(TEXT("\nКористувач: %s"), name);
    return 0;
}
int GetDir() {
    char SystemDir[100];
    GetSystemDirectory(SystemDir, 100);
    _tprintf(TEXT("\nДиректорія системи: %s"), SystemDir);
    char WindowsDir[100];
    GetWindowsDirectory(WindowsDir, 100);
    _tprintf(TEXT("\nДиректорія Вінди: %s"), WindowsDir);
    char TmpDir[100];
    GetTempPath(100, TmpDir);
    _tprintf(TEXT("\nТимчасова директорія: %s"), TmpDir);
    return 0;
}
int GetChangeOfDir(vector<string>& drives) {
    GetDriveLetter(drives);
    for (int i = 0; i < drives.size(); i++) {
        cout << (i+1) << ".Диск: " + drives[i] << endl;
    }
    cout << endl << "Оберіть директорію: ";
    char l, dir[10];
    bool s = TRUE;
    while (s) {
        cin >> l;
        switch (l) {
        case '1':
            strcpy_s(dir, drives[0].c_str());
            s = FALSE;
            break;
        case '2':
            strcpy_s(dir, drives[1].c_str());
            s = FALSE;
            break;
        case '3':
            strcpy_s(dir, drives[2].c_str());
            s = FALSE;
            break;
        default:
            cout << "Оберіть іншу\n" << endl;
        }
    }
    cout << endl << "Оберіть файл:" << dir << endl;
    HANDLE notif = FindFirstChangeNotification(dir,TRUE, FILE_NOTIFY_CHANGE_SIZE);
    cout << "Нажміть space для завершення" << endl;
    bool p = TRUE;
    while (p) {
        cout << "WAINTING..." << endl;
        switch (WaitForSingleObject(notif, 5000))
        {
        case WAIT_ABANDONED:
        case WAIT_FAILED:
        {
            break;
        }
        case WAIT_OBJECT_0:
        {
            cout << "Розмір директорії змінено" << endl;
            break;
        }
        }
        FindNextChangeNotification(notif);
        cout << endl;
        if (GetKeyState(VK_SPACE) & 0x8000)
        {
            printf("SPACE\n");
            p = FALSE;
        }
    }
    return 0;
}
int main()
{  
    bool s = TRUE;
    while (s) {
        vector<string> drives;
        printf("1.Список усіх логічних дисків в системі\n");
        printf("2.Отримати тип кожного диску присутнього в системі, та дати пояснення для кожного типу диску\n");
        printf("3.Отримати інформацію про диски в системі та про файлові системи Які Використовують на них\n");
        printf("4.Отримати інформацію про зайнятості та вільне місце на кожному з дисків\n");
        printf("5.Отримати інформацію про системну пам'ять\n");
        printf("6.Отримати інформацію про Назву комп'ютера\n");
        printf("7.Отримати Назву поточного користувача\n");
        printf("8.Отримати інформацію про поточний системний каталог, Тимчасовий каталог, поточний робочий каталог\n");
        printf("9.Для обраних каталогу на диску, Включити спостереження за змінами, проде-монструвати відслідковування більше однієї зміни\n");
        printf("0.Вихід\n");
        bool k = TRUE;
        while (k) {
            char num;
            cout << endl << "Виберіть номер:: ";
            cin >> num;
            cout << endl;
            switch (num) {
            case '0':
                s = FALSE;
                k = FALSE;
                break;
            case '1':
                GetDriveLetter(drives);
                for (string x : drives) {
                    cout << "Диск: " + x << endl;
                }
                k = FALSE;
                break;
            case '2':
                GetDrivesType(drives);
                k = FALSE;
                break;
            case '3':
                GetInfoAboutDrivers(drives);
                k = FALSE;
                break;
            case '4':
                GetInfoAboutFreeSpace(drives);
                k = FALSE;
                break;
            case '5':
                GetInfoAboutSystemMemory(drives);
                k = FALSE;
                break;
            case '6':
                GetCompName();
                k = FALSE;
                break;
            case '7':
                GetUsName();
                k = FALSE;
                break;
            case '8':
                GetDir();
                k = FALSE;
                break;
            case '9':
                GetChangeOfDir(drives);
                k = FALSE;
                break;
            default:
                cout << "Оберіть іншу\n" << endl;
            }
        }
        if (s == TRUE) {
            string temp;
            cin >> temp;
            system("cls");
        }
    }
}
